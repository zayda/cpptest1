#include <string>
using namespace std;

namespace http {
    class Server {
    public:
        Server(string ip_address, int port);
        void run();
        void stop();

    private:
        int server_socket;
        struct sockaddr_in socket_address;
        unsigned int socket_address_len;

        string buildResponse(string body);
    };
}