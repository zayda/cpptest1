#include <string>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "Server.h"
using namespace std;

namespace http {

    const int BUFFER_SIZE = 30720;
    const int MAX_PENDING_CONNECTIONS = 5;

    void exitWithError(string errorMessage) {
        printf("ERROR: %s\n", errorMessage.c_str());
        exit(1);
    }

    Server::Server(string ip_address, int port) : server_socket(), socket_address(), socket_address_len(sizeof(socket_address)) {
        socket_address.sin_family = AF_INET;
        socket_address.sin_port = htons(port);
        socket_address.sin_addr.s_addr = inet_addr(ip_address.c_str());
        server_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (server_socket < 0) {
            exitWithError("Cannot create socket");
        }
        int status = ::bind(server_socket, (sockaddr *) &socket_address, socket_address_len);
        if (status != 0) {
            exitWithError("Cannot bind to socket");
        }
        if (listen(server_socket, MAX_PENDING_CONNECTIONS) < 0) {
            exitWithError("Cannot listen to socket");
        }
        ostringstream ss;
        ss << "Listening on " << inet_ntoa(socket_address.sin_addr) << ":" << ntohs(socket_address.sin_port);
        printf("%s\n", ss.str().c_str());
    }

    void Server::run() {
        while (true) {
            int accepted_socket = accept(server_socket, (sockaddr *) &socket_address, &socket_address_len);
            if (accepted_socket < 0) {
                exitWithError("Failed to accept incoming connection");
            }
            char buffer[BUFFER_SIZE] = {0};
            long readCount = read(accepted_socket, buffer, BUFFER_SIZE);
            if (readCount < 0) {
                exitWithError("Failed to read bytes from client accepted_socket connection");
            }
            printf("%lu byte(s) read > ", readCount);

            const auto p1 = std::chrono::system_clock::now();
            ostringstream ss;
            ss << "Hello " << std::chrono::duration_cast<std::chrono::seconds>(p1.time_since_epoch()).count();
            string response = buildResponse(ss.str());
            long writeCount = write(accepted_socket, response.c_str(), response.size());
            printf("%lu byte(s) written\n", writeCount);
            close(accepted_socket);
        }
    }

    void Server::stop() {
        close(server_socket);
        printf("Server closed\n");
    }

    string Server::buildResponse(string body) {
        ostringstream ss;
        ss << "HTTP/1.1 200 OK\n";
        ss << "Content-Type: text/plain\n";
        ss << "Content-Length: " << body.size() << "\n\n";
        ss << body;
        return ss.str();
    }
}