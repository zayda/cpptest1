#include <csignal>
#include "http/Server.cpp"

using namespace http;

Server server("127.0.0.1", 8080);

void signalHandler(int signum) {
    printf("Interrupt signal %u received...\n", signum);
    server.stop();
    exit(signum);
}

int main() {
    signal(SIGTERM, signalHandler);
    server.run();
    return 0;
}
